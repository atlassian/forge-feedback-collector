import api, { route } from "@forge/api";
import Resolver from "@forge/resolver";

import { Config } from "../";
import { buildDescriptionAdf } from "./utils";

const resolver = new Resolver();

/*
 * Defines a function to fetch the accessible projects and issue types
 * from Jira.
 */
resolver.define("getJiraProjectsAndIssueTypes", async (_) => {
  // NOTE: Pagination is not being handled in this example for brevity.
  const response = await api
    .asApp()
    .requestJira(route`/rest/api/3/project/search?expand=issueTypes`, {
      headers: {
        Accept: "application/json",
      },
    });

  if (!response.ok) {
    throw new Error(
      "Failed to fetch Jira projects and related issue types. " +
        "Ensure this app has been installed on at least one Jira instance. " +
        "E.g 'forge install -p Jira'",
    );
  }

  const body = await response.json();

  // Ignore subtask issue types.
  const data = body.values.map((project) => {
    project.issueTypes = project.issueTypes.filter(
      (issueType) => !issueType.subtask,
    );
    return project;
  });

  return data;
});

/*
 * Defines a function to submit provided feedback as a new issue in
 * the configured Jira project.
 */
resolver.define("submitFeedbackToJira", async (req) => {
  const {
    payload: { summary, description },
    context: {
      accountId,
      // Module-specific context data.
      // https://developer.atlassian.com/platform/forge/runtime-reference/forge-resolver/#arguments
      extension: { content, config, space },
      siteUrl,
    },
  } = req;

  const documentLink = `${siteUrl}/wiki/spaces/${space.key}/pages/${content.id}`;
  const { projectId, issueTypeId, assigneeId, reporterType } = config as Config;
  const reporter =
    reporterType === "author" ? { reporter: { id: accountId } } : {};

  var bodyData = {
    fields: {
      assignee: { id: assigneeId },
      ...reporter,
      summary,
      description: buildDescriptionAdf(documentLink, description),
      issuetype: { id: issueTypeId },
      project: { id: projectId },
    },
  };

  const response = await api.asApp().requestJira(route`/rest/api/3/issue`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(bodyData),
  });

  if (!response.ok) {
    throw new Error(
      "Failed to create an issue in Jira. Ensure the configuration is up to date.",
    );
  }

  // Return the key of the newly created issue.
  return (await response.json()).key;
});

export const handler = resolver.getDefinitions();
