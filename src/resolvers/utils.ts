// https://developer.atlassian.com/cloud/jira/platform/apis/document/structure/
export const buildDescriptionAdf = (link: string, description?: string) => {
  const adf = {
    content: [
      {
        content: [
          {
            text: "Feedback submitted for ",
            type: "text",
            marks: [
              {
                type: "em",
              },
            ],
          },
          {
            type: "inlineCard",
            attrs: {
              url: link,
            },
          },
        ],
        type: "paragraph",
      },
    ],
    type: "doc",
    version: 1,
  } as any;

  if (description) {
    adf.content.push({
      type: "blockquote",
      content: [
        {
          type: "paragraph",
          content: [
            {
              text: description,
              type: "text",
            },
          ],
        },
      ],
    });
  }

  return adf;
};
