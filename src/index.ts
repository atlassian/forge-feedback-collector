export { handler } from "./resolvers";

export type Config = {
  projectId: string;
  issueTypeId: string;
  assigneeId: string;
  reporterType: "author" | "anonymous";
};
