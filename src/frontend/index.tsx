import React, { useState } from "react";

import Forge, {
  Button,
  Link,
  SectionMessage,
  SectionMessageAction,
  Text,
} from "@forge/react";
import FeedbackForm from "./FeedbackForm";

type Stage = "prompt" | "editing-feedback" | "success";

const App = () => {
  const [stage, setStage] = useState<Stage>("prompt");
  const [newIssueKey, setNewIssueKey] = useState<string | null>(null);

  if (stage === "editing-feedback") {
    return (
      <FeedbackForm
        onFeedbackCompleted={(issueKey) => {
          setNewIssueKey(issueKey);
          setStage("success");
        }}
        onFormCancelled={() => setStage("prompt")}
      />
    );
  }

  if (stage === "success") {
    return (
      <SectionMessage
        appearance="success"
        actions={[
          <SectionMessageAction onClick={() => setStage("editing-feedback")}>
            Give more feedback
          </SectionMessageAction>,
          <SectionMessageAction onClick={() => setStage("prompt")}>
            Dismiss
          </SectionMessageAction>,
        ]}
      >
        <Text>
          Issue created{" "}
          <Link href={`/browse/${newIssueKey}`} openNewTab>
            {newIssueKey}
          </Link>
          . Thank you for your feedback! 🙏
        </Text>
      </SectionMessage>
    );
  }

  return (
    <Button onClick={() => setStage("editing-feedback")}>
      📣 Leave feedback on this page
    </Button>
  );
};

Forge.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
