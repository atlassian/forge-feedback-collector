import React, { useEffect } from "react";

import {
  Box,
  Button,
  ButtonGroup,
  Form,
  FormFooter,
  FormHeader,
  FormSection,
  Label,
  LoadingButton,
  RequiredAsterisk,
  SectionMessage,
  Stack,
  Strong,
  Text,
  TextArea,
  Textfield,
  useForm,
  xcss,
} from "@forge/react";
import useFeedbackSubmission from "./useFeedbackSubmission";

const formContainerStyle = xcss({
  backgroundColor: "elevation.surface.sunken",
  padding: "space.200",
  borderColor: "color.border",
  borderWidth: "border.width",
  borderStyle: "solid",
  borderRadius: "border.radius",
  ":hover": {
    backgroundColor: "elevation.surface",
  },
});

type FormProps = {
  onFormCancelled: () => void;
  onFeedbackCompleted: (issueKey: string) => void;
};

const FeedbackForm = ({ onFormCancelled, onFeedbackCompleted }: FormProps) => {
  const { getFieldId, register, handleSubmit } = useForm();
  const { submit, loading, result, issueKey } = useFeedbackSubmission();

  useEffect(() => {
    if (result !== "success") return;
    onFeedbackCompleted(issueKey);
  }, [result]);

  return (
    <Box xcss={formContainerStyle}>
      <Form onSubmit={handleSubmit(submit)}>
        <FormHeader title="Leave feedback">
          Provide your feedback below. Required fields are marked an asterisk
          <RequiredAsterisk />
        </FormHeader>
        <FormSection>
          <Stack space="space.100">
            {result instanceof Error && (
              <SectionMessage appearance="error">
                <Text>
                  <Strong> {`Error: ${result.message}`}</Strong>
                </Text>
              </SectionMessage>
            )}
            <Box>
              <Label labelFor={getFieldId("summary")}>
                Summary <RequiredAsterisk />
              </Label>
              <Textfield
                {...register("summary", { required: true })}
                isDisabled={loading}
              />
            </Box>
            <Box>
              <Label labelFor={getFieldId("description")}>Description</Label>
              <TextArea {...register("description")} isDisabled={loading} />
            </Box>
          </Stack>
        </FormSection>
        <FormFooter>
          <ButtonGroup label="Feedback form actions">
            <Button
              isDisabled={loading}
              appearance="subtle"
              type="reset"
              onClick={onFormCancelled}
            >
              Cancel
            </Button>
            <LoadingButton
              isLoading={loading}
              appearance="primary"
              type="submit"
            >
              Submit
            </LoadingButton>
          </ButtonGroup>
        </FormFooter>
      </Form>
    </Box>
  );
};

export default FeedbackForm;
