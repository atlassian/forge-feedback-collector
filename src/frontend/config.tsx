import React from "react";

import Forge, {
  Box,
  Button,
  Form,
  FormFooter,
  FormHeader,
  FormSection,
  HelperMessage,
  Label,
  RadioGroup,
  RequiredAsterisk,
  SectionMessage,
  Select,
  Stack,
  Text,
  UserPicker,
  useForm,
} from "@forge/react";
import { view } from "@forge/bridge";

import { type Config } from "../";
import useJiraProjectData from "./useJiraProjectData";

const Config = () => {
  const { projects, loading, error } = useJiraProjectData();
  const { handleSubmit, register, getFieldId, getValues } = useForm();

  const selectedProjectId = getValues("project.value");
  const selectedProject = projects?.find(({ id }) => id === selectedProjectId);

  const projectOptions =
    projects?.map((project) => ({
      label: `${project.name} (${project.key})`,
      value: project.id,
    })) ?? [];

  const issueTypeOptions =
    selectedProject?.issueTypes.map((issueType) => ({
      label: issueType.name,
      value: issueType.id,
    })) ?? [];

  const submitConfig = (config: any) => {
    // Finalize the shape of the config. This is the object that will be
    // available to the view and resolvers. Submit closes the modal and
    // loads the main app.
    view.submit({
      config: {
        projectId: config.project.value,
        issueTypeId: config.issueType.value,
        assigneeId: config.assignee.id,
        reporterType: config.reporterType,
      } as Config,
    });
  };

  if (error) {
    return (
      <SectionMessage appearance="error">
        <Text>{error}</Text>
      </SectionMessage>
    );
  }

  return (
    <Form onSubmit={handleSubmit(submitConfig)}>
      <FormHeader>
        Complete the form to enable feedback on this document. Required fields
        are marked with an asterisk <RequiredAsterisk />
      </FormHeader>
      <FormSection>
        <Stack space="space.100">
          <Box>
            <Label labelFor={getFieldId("project")}>
              Project
              <RequiredAsterisk />
            </Label>
            <Select
              isLoading={loading}
              options={projectOptions}
              {...register("project", { required: true })}
            />
          </Box>
          <Box>
            <Label labelFor={getFieldId("issueType")}>
              Issue type
              <RequiredAsterisk />
            </Label>
            <Select
              {...register("issueType", { required: true })}
              isLoading={loading}
              options={issueTypeOptions}
            />
            <HelperMessage>
              Feedback will appear as new issues in the selected project with
              the selected issue type.
            </HelperMessage>
          </Box>
          <Box>
            <UserPicker
              label="Assign issues to"
              isRequired
              {...register("assignee", { required: true })}
            />
          </Box>
          <Box>
            <Label labelFor={getFieldId("reporterType")}>
              Set issue reporter as <RequiredAsterisk />
            </Label>
            <RadioGroup
              isRequired
              options={[
                {
                  label: "Feedback author",
                  value: "author",
                },
                {
                  label: "Anonymous",
                  value: "anonymous",
                },
              ]}
              {...register("reporterType", { required: true })}
            />
          </Box>
        </Stack>
      </FormSection>
      <FormFooter>
        <Button appearance="primary" type="submit">
          Apply
        </Button>
      </FormFooter>
    </Form>
  );
};

Forge.render(
  <React.StrictMode>
    <Config />
  </React.StrictMode>,
);

export default Config;
