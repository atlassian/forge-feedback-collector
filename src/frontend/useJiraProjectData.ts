import { invoke } from "@forge/bridge";
import { useEffect, useState } from "react";

const useJiraProjectData = () => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    invoke("getJiraProjectsAndIssueTypes")
      .then(setData)
      .catch((error: Error) => setError(error.message))
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return {
    projects: data,
    loading,
    error,
  };
};

export default useJiraProjectData;
