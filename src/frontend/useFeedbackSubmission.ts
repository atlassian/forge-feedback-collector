import { useState } from "react";
import { invoke } from "@forge/bridge";

type FeedbackPayload = {
  summary: string;
  description?: string;
};

type FeedbackSubmissionHelper = {
  result: null | "success" | Error;
  loading: boolean;
  submit: (payload: FeedbackPayload) => void;
  issueKey?: string;
};

export const useFeedbackSubmission = (): FeedbackSubmissionHelper => {
  const [result, setResult] = useState(null);
  const [loading, setLoading] = useState(false);
  const [issueKey, setIssueKey] = useState(null);

  const submit = async (payload: FeedbackPayload) => {
    setLoading(true);
    setResult(null);
    setIssueKey(null);

    try {
      const issueKey = await invoke("submitFeedbackToJira", payload);
      setIssueKey(issueKey);
      setResult("success");
    } catch (_) {
      setResult(
        new Error("Failed to submit your feedback - try again shortly."),
      );
    } finally {
      setLoading(false);
    }
  };

  return { result, loading, submit, issueKey };
};

export default useFeedbackSubmission;
